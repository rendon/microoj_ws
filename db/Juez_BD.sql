-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: Juez_BD
-- ------------------------------------------------------
-- Server version	5.5.31-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `Juez_BD`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `Juez_BD` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `Juez_BD`;

--
-- Table structure for table `CasoDePrueba`
--

DROP TABLE IF EXISTS `CasoDePrueba`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CasoDePrueba` (
  `idCasoDePrueba` int(11) NOT NULL AUTO_INCREMENT,
  `idProblema` int(11) NOT NULL,
  `entrada` text,
  `salida` text NOT NULL,
  PRIMARY KEY (`idCasoDePrueba`),
  UNIQUE KEY `unique_problem_tc` (`idCasoDePrueba`,`idProblema`),
  KEY `fk_CasoDePrueba_Problema` (`idProblema`),
  CONSTRAINT `fk_CasoDePrueba_Problema` FOREIGN KEY (`idProblema`) REFERENCES `Problema` (`idProblema`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CasoDePrueba`
--

LOCK TABLES `CasoDePrueba` WRITE;
/*!40000 ALTER TABLE `CasoDePrueba` DISABLE KEYS */;
INSERT INTO `CasoDePrueba` VALUES (1,1,'55','1540'),(2,1,'484','117370'),(3,1,'476','113526'),(4,1,'604351','182620367776'),(5,1,'902043','406841237946'),(6,2,'anitalavalatina','Si'),(7,2,'qwertyuioppoiuytrewq','Si'),(8,2,'ququ23iz2as2h2aze2scuch23adosobr7elos9p4alnd2romo2son2cade2nasdetextoqueseleenigualdeinzquierdaaderechaquedederechaainzquierdaorejemplolacadenaanitalavalatinaesunpalndromomientrasqueotraspalabrasnoloesnesteprobl111ematiene111squ11edet1ermi42124narsiuna4444ca44d44enade4t4exto6eso4n88ou6npa3lnd2rom2o23qu23iz2as2h2aze2scuch23adosobr7elos9p4alnd2romo2son2cade2nasdetextoqueseleenigualdeinzquierdaaderechaquedederechaainzquierdaorejemplolacadenaanitalavalatinaesunpalndromomientrasqueotraspalabrasnoloesnesteprobl111ematiene111squ11edet1ermi42124narsiuna4444ca44d44enade4t4exto6eso4n88ou6npa3lnd2rom2oizqu23iz2as2h2aze2scuch23adosobr7elos9p4alnd2romo2son2cade2nasdetextoqueseleenigualdeinzquierdaaderechaquedederechaainzquierdaorejemplolacadenaanitalavalatinaesunpalndromomientrasqueotraspalabrasnoloesnesteprobl111ematiene111squ11edet1ermi42124narsiuna4444ca44d44enade4t4exto6eso4n88ou6npa3lnd2rom2o2aqu23iz2as2h2aze2scuch23adosobr7elos9p4alnd2romo2son2cade2nasdetextoqueseleenigualdeinzquierdaaderecha','No'),(9,2,'a','Si'),(10,2,'0xFFAAFFx0','Si'),(11,3,'0','0'),(12,3,'9223372036854775808','9 2 2 3 3 7 2 0 3 6 8 5 4 7 7 5 8 0 8'),(13,3,'123','1 2 3'),(14,3,'1','1'),(15,3,'753421','7 5 3 4 2 1');
/*!40000 ALTER TABLE `CasoDePrueba` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Error`
--

DROP TABLE IF EXISTS `Error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Error` (
  `idError` int(11) NOT NULL,
  `mensaje` text,
  PRIMARY KEY (`idError`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Error`
--

LOCK TABLES `Error` WRITE;
/*!40000 ALTER TABLE `Error` DISABLE KEYS */;
INSERT INTO `Error` VALUES (1,'');
/*!40000 ALTER TABLE `Error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Problema`
--

DROP TABLE IF EXISTS `Problema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Problema` (
  `idProblema` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `descripcion` text NOT NULL,
  `autor` int(11) NOT NULL,
  PRIMARY KEY (`idProblema`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_Problema_Usuario1` (`autor`),
  CONSTRAINT `fk_Problema_Usuario1` FOREIGN KEY (`autor`) REFERENCES `Usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Problema`
--

LOCK TABLES `Problema` WRITE;
/*!40000 ALTER TABLE `Problema` DISABLE KEYS */;
INSERT INTO `Problema` VALUES (1,'Bienvenida','Haz decidido unirte al equipo de programadores que esta entrenando para participar en CONACUP este año y para ello el resto de los integrantes han diseñado un problema especialmente para ti y que servirá para ver si cuentas con lo mínimo para entrar, no te preocupes, no es tan difícil.\n\nDado un número N (1 &lt;= N &lt;= 1,000,000), imprime la suma de los números del 1 hasta N (inclusive).\n\n###Ejemplo de entrada.\n\n    5 \n\n###Ejemplo de salida:\n\n    15\n\n**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de líneas.\n',1),(2,'Anita Lava la Tina','Quizás haz escuchado sobre los palíndromos. Son cadenas de texto que se leen\nigual de inzquierda a derecha que de derecha a inzquierda. Por ejemplo, la\ncadena &quot;anitalavalatina&quot; es un palíndromo, mientras que &quot;otraspalabras&quot; no lo\nes.\n\nEn este problema tienes que determinar si una cadena de texto es o no un palíndromo.\n\n###Entrada\nLa entrada consiste de una cadena de texto con una longitud N (1 &lt;= N &lt;= 1000),\nque consisten únicamente de letras minúsculas y números.\n\n###Salida\nSi la cadena es palíndroma debes imprimir la cadeda &quot;Si&quot; (sin comillas), de lo\ncontrario debes imprimir &quot;No&quot; (sin las comillas).\n\n###Ejemplo de entrada\n\n    asdfghjkllkjhgfdsa\n\n###Ejemplo de salida\n\n    Si\n\n\n**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de líneas extras.\n',1),(3,'Separando Dígitos','Dado un número entero N (0 &lt;= N &lt;= 2^63) imprime sus dígitos separados por un espacio.\n\n###Ejemplo de entrada\n\n    12345\n\n###Ejemplo de salida\n\n    1 2 3 4 5\n\n\n**NOTA**: Imprimir únicamente lo que se pide, no espacios ni saltos de líneas extras.\n',1);
/*!40000 ALTER TABLE `Problema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Solucion`
--

DROP TABLE IF EXISTS `Solucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Solucion` (
  `idSolucion` int(11) NOT NULL AUTO_INCREMENT,
  `idProblema` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `codigoFuente` text NOT NULL,
  `lenguaje` int(11) NOT NULL,
  PRIMARY KEY (`idSolucion`),
  KEY `fk_Solucion_Problema1` (`idProblema`),
  KEY `fk_Solucion_Usuario1` (`idUsuario`),
  CONSTRAINT `fk_Solucion_Problema1` FOREIGN KEY (`idProblema`) REFERENCES `Problema` (`idProblema`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Solucion_Usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Solucion`
--

LOCK TABLES `Solucion` WRITE;
/*!40000 ALTER TABLE `Solucion` DISABLE KEYS */;
INSERT INTO `Solucion` VALUES (1,1,2,'#include <stdio.h>\n\nint main(int argc, char **argv)\n{\n	int n;\n	scanf(\"%d\", &n);\n	printf(\"%d\", n * (n + 1) / 2);\n	return 0;\n}\n',1),(2,1,2,'#include <stdio.h>\n\nint main(int argc, char **argv)\n{\n	int n;\n	scanf(\"%d\", &n);\n	printf(\"%d\", n * (n + 1) / 2);\n	return 0;\n}\n',1),(3,1,2,'#include <stdio.h>\n\nint main(int argc, char **argv)\n{\n	int n;\n	scanf(\"%d\", &n);\n	printf(\"%d\", n * (n + 1) / 2);\n	return 0;\n}\n',1),(4,1,2,'#include <stdio.h>\n\nint main(int argc, char **argv)\n{\n	long long n;\n	scanf(\"%lld\", &n);\n	printf(\"%lld\", n * (n + 1) / 2);\n	return 0;\n}\n',1),(5,1,2,'#include <iostream>\nusing namespace std;\nint main(int argc, char **argv)\n{\n	long long n;\n	cin >> n;\n	cout << n * (n + 1) / 2 << endl;\n	return 0;\n}\n',2),(6,1,2,'import sys\n\nn = int(raw_input())\n\nprint n * (n + 1) / 2\n',3),(7,2,2,'#include <stdio.h>\n#include <string.h>\n\nint main(int argc, char **argv)\n{\n    int i, j, n;\n    char S[1005];\n    scanf(\"%s\", S);\n    n = strlen(S);\n    i = 0;\n    j = n - 1;\n    \n    int palindrome = 1;\n    while (i < j) {\n        if (S[i] != S[j])\n        palindrome = 0;\n    }\n    \n    if (palindrome)\n        printf(\"Si\");\n    else\n        printf(\"No\");    \n    \n	return 0;\n}\n',1),(8,2,2,'#include <stdio.h>\n#include <string.h>\n\nint main(int argc, char **argv)\n{\n    int i, j, n;\n    char S[1005];\n    scanf(\"%s\", S);\n    n = strlen(S);\n    i = 0;\n    j = n - 1;\n    \n    int palindrome = 1;\n    while (i < j) {\n        if (S[i] != S[j])\n        palindrome = 0;\n        i++;\n        j--;\n    }\n    \n    if (palindrome)\n        printf(\"Si\");\n    else\n        printf(\"No\");    \n    \n	return 0;\n}\n',1),(9,2,2,'#include <iostream>\n#include <algorithm>\nusing namespace std;\nint main(int argc, char **argv)\n{\n	string A, B;\n	cin >> A;\n	B = A;\n	reverse(B.begin(), B.end());\n	\n	if (A == B)\n	    cout << \"Si\";\n    else\n	    cout << \"No\";\n	\n	return 0;\n}\n',2),(10,2,2,'import sys\n\nA = raw_input()\nB = B[::-1]\n\nif A == B:\n    print \"Si\"\nelse\n    print \"No\"\n',3),(11,2,2,'import sys\n\nA = raw_input()\nB = A[::-1]\n\nif A == B:\n    print \"Si\"\nelse:\n    print \"No\"\n',3),(12,3,2,'import sys\n\nn = raw_input()\ndigits = list(n)\n\nfor d in digits:\n    print d,\n',3);
/*!40000 ALTER TABLE `Solucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  `contrasenia` varchar(16) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,'administrador','sdflkj','3c3f90f8c30f2b8d3bd8c9f7983166fc3735483a'),(2,'rendon','weroiu','46caa746976eb9cae93ae51830dd25dc15d0e3da');
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-07 20:26:56
