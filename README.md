MicroOJ WS
==========
A simple Web Service that allows the clients to store programming contest problems, its solutions (source code), compile the solution and report the results. 
This is college project.

This Web Service is coupled with my other probject [MicroOJ](https://bitbucket.org/rendon/microoj) (the client).

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=865](http://rendon.x10.mx/?p=865).

License
=======
With exception of the employed components (wich have it's own licenses), this project is under GPLv3.
