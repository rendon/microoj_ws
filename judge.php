<?php
require_once 'lib/nusoap.php';
define('HOST', 'localhost');
define('USER', 'root');
define('USER_PASS', 'mysqlroot');
define('DB_NAME', 'Juez_BD');

# MySQL error codes.
define('ER_DUP_ENTRY', -1062);

define('ER_FAILED_TO_INSERT_RECORD', -1);
define('ER_AUTHENTICATION_FAILED', -2);
define('ER_TITLE_ALREADY_TAKEN', -3);

# Languages.
define('LANG_C', 1);
define('LANG_CPP', 2);
define('LANG_PYTHON', 3);

# Compilation constants.
define('SU_COMPILATION', 0);
define('ER_COMPILATION', 1);
define('NO_SUCH_SOLUTION', 2);

# Results.
define('AC', 0);
define('WA', 1);
define('TLE', 2);

define('VALID_USER', -100);
define('INVALID_USER', -101);

define('ADMIN_USER', 'administrador');

class Judge
{
    private function fius($name, $password, $remember_token)
    {
        $query  = "INSERT INTO Usuario(nombre, contrasenia, remember_token) "
                . "VALUES('" . utf8_encode(addslashes($name)) . "', '"
                . utf8_encode(addslashes($password)) . "', '" 
                . $remember_token . "')";

        return $query;
    }

    /** Format Insert Problem Statement */
    private function fips($title, $description, $user_id)
    {
        $query  = "INSERT INTO Problema(titulo, descripcion, autor) "
                . "VALUES('" . utf8_encode(addslashes($title)) . "', '"
                . utf8_encode(addslashes($description)) . "', "
                . $user_id . ")";

        return $query;
    }

    // Format Insert Solution Statement
    private function fiss($id_problem, $id_user, $source_code, $language)
    {
        $query  = "INSERT INTO Solucion(idProblema, idUsuario, "
                . "codigoFuente, lenguaje) VALUES(" . $id_problem . ", "
                . $id_user . ", \"" . addslashes($source_code)
                . "\", " . $language . ")";
        return $query;
    }

    // Format Insert Test Case Statement
    private function fitcs($id_problem, $input, $output)
    {
        $query  = "INSERT INTO CasoDePrueba(idProblema, entrada, salida) "
                . "VALUES(" . $id_problem . ", '"
                . utf8_encode(addslashes($input)) . "', '"
                . utf8_encode(addslashes($output)) . "')";

        return $query;
    }


    private function log($msg)
    {
        $mysqli = $this->open_db();
        
        # We only need the last error
        $query = "DELETE FROM Error";
        $mysqli->query($query);

        $query  = "INSERT INTO Error(idError, mensaje) "
                . "VALUES(1, '" . addslashes($msg) . "')";
        $mysqli->query($query);
        $mysqli->close();
    }

    private function open_db()
    {
        $connection = new mysqli(HOST, USER, USER_PASS, DB_NAME);
        if ($connection->connect_errno) {
            //echo "Failed to connect to the server: " . 
                 //$connection->connect_errno . "\n";
        }

        return $connection;
    }

    private function find_user($user, $password, $mysqli)
    {
        $query  = "SELECT idUsuario FROM Usuario "
                . "WHERE nombre = '" . addslashes($user) ."'"
               . " AND contrasenia = '" . addslashes($password) . "'";
        $result = $mysqli->query($query);
        if ($result->num_rows == 0) {
            return 0;
        }

        $row = $result->fetch_assoc();
        $id = $row["idUsuario"];

        return $id;
    }


    function create_problem($user, $password, $title, $description, $author)
    {
        if ($this->authenticate_user($user, $password) != VALID_USER) {
            return INVALID_USER;
        }

        $mysqli = $this->open_db();
        $query = $this->fips($title,
                             $description,
                             $author);

        $mysqli->query("SET NAMES UTF8");
        $mysqli->query($query);
        $code = $mysqli->insert_id;

        if ($code == 0) {
            if ($mysqli->errno == ER_DUP_ENTRY) {
                return ER_TITLE_ALREADY_TAKEN;
            } else {
                return ER_FAILED_TO_INSERT_RECORD;
            }
        }

        $mysqli->close();
        return $code;
    }

    function get_problem($id_problem)
    {
        $mysqli = $this->open_db();
        $query = "SELECT idProblema, titulo, descripcion, nombre as creador "
               . "FROM Problema, Usuario "
               . "WHERE idProblema = " . $id_problem . " AND "
               . "autor = idUsuario";

        $result = $mysqli->query($query);
        if ($result->num_rows == 0) {
            $mysqli->close();
            return array();
        }

        $row = $result->fetch_assoc();
        $problem = array(
                    'id'            => $row['idProblema'],
                    'title'         => $row['titulo'],
                    'description'   => $row['descripcion']
                );


        $mysqli->close();
        return $problem;
    }

    function get_all_problems()
    {
        $mysqli = $this->open_db();
        $query = "SELECT idProblema, titulo, descripcion, nombre as creador "
               . "FROM Problema, Usuario "
               . "WHERE autor = idUsuario";
        $result = $mysqli->query($query);

        $problems = array();
        while ($row = $result->fetch_assoc()) {
            $problems[] = array(
                'id' => $row['idProblema'],
                'title' => $row['titulo'],
                'description' => $row['descripcion']
            );
        }

        return $problems;
    }

    function save_solution($user, $password, $id_problem,
                           $source_code, $language)
    {
        if ($this->authenticate_user($user, $password) != VALID_USER) {
            return array(
                'id' => 0,
                'error' => INVALID_USER
            );
        }

        $mysqli = $this->open_db();
        $id_user = $this->find_user($user, $password, $mysqli);
        $mysqli->query("SET NAMES UTF8");
        $query = $this->fiss($id_problem, $id_user, $source_code, $language);
        $mysqli->query($query);
        $id = $mysqli->insert_id;

        if ($id == 0) {
            return array(
                'id' => 0,
                'error' => ER_FAILED_TO_INSERT_RECORD
            );
        }

        if ($this->compile_solution($user, $password, $id) != 0) {
            return array(
                'id' => $id,
                'error' => ER_COMPILATION
            );
        }

        $mysqli->close();
        return array(
            'id' => $id,
            'error' => SU_COMPILATION
        );
    }

    function compile_solution($user, $password, $id_solution)
    {
        if ($this->authenticate_user($user, $password) != VALID_USER) {
            return INVALID_USER;
        }

        $mysqli = $this->open_db();
        $query  = "SELECT codigoFuente, lenguaje FROM Solucion WHERE "
                . "idSolucion = " . $id_solution;

        $result = $mysqli->query($query);
        if ($result->num_rows == 0) {
            $mysqli->close();
            return NO_SUCH_SOLUTION;
        }
        
        $row = $result->fetch_assoc();
        $lang = $row["lenguaje"];
        $source = $row["codigoFuente"];
        $mysqli->close();

        $exec_file = tempnam("/tmp", "sol_");
        $source_file = $exec_file;
        $command = "";

        if ($lang == LANG_C) {
            $source_file .= ".c";
            $command = "gcc -Wall -o " . $exec_file
                     . " " . $source_file . " 2>&1";
        } else if ($lang == LANG_CPP) {
            $source_file .= ".cpp";
            $command = "g++ -Wall -o " . $exec_file
                     . " " . $source_file . " 2>&1";
        } else if ($lang == LANG_PYTHON) {
            $source_file .= ".py";
            $command = "python -m py_compile " . $source_file . " 2>&1";
        }

        rename($exec_file, $source_file);

        $fd = fopen($source_file, "w");
        fwrite($fd, $source);
        fclose($fd);

        $output = array();
        $exit_code = 0;
        $str = exec($command, $output, $exit_code);

        $msg = "";
        foreach ($output as $line) {
            $msg .= $line . "\n";
        }
        $this->log($msg);


        unlink($source_file);
        unlink($exec_file);
        if ($exit_code == 0) {
            return SU_COMPILATION;
        } else {
            return ER_COMPILATION;
        }
    }

    // TODO: Error by user.
    function get_last_error()
    {
        $mysqli = $this->open_db();
        $query = "SELECT * FROM Error";
        $result = $mysqli->query($query);

        if ($result->num_rows == 0) {
            return "";
        }

        $row = $result->fetch_assoc();
        $msg = $row["mensaje"];
        return "Error: " . $msg;
    }

    function save_test_case($user, $password, $id_problem, $input, $output)
    {
        if ($user != ADMIN_USER || 
            $this->authenticate_user(ADMIN_USER, $password) != VALID_USER) {
            return INVALID_USER;
        }

        $mysqli = $this->open_db();
        $query = $this->fitcs($id_problem, $input, $output);
        $mysqli->query("SET NAMES UTF8");
        $mysqli->query($query);
        $id = $mysqli->insert_id;

        if ($id == 0) {
            return ER_FAILED_TO_INSERT_RECORD;
        } 

        $mysqli->close();
        return $id;
    }

    // $source_code contains a valid source code.
    private function compile_program($source_code, $lang)
    {
        $exec_file = tempnam("/tmp", "sol_");
        $source_file = $exec_file;
        $command = "";

        if ($lang == LANG_C) {
            $source_file .= ".c";
            $command = "gcc -Wall -o " . $exec_file
                     . " " . $source_file . " 2>&1";
        } else if ($lang == LANG_CPP) {
            $source_file .= ".cpp";
            $command = "g++ -Wall -o " . $exec_file
                     . " " . $source_file . " 2>&1";
        } else if ($lang == LANG_PYTHON) {
            $source_file .= ".py";
        }

        rename($exec_file, $source_file);

        $fd = fopen($source_file, "w");
        fwrite($fd, $source_code);
        fclose($fd);

        if ($lang == LANG_C || $lang == LANG_CPP) {
            exec($command);
            return $exec_file;
        } else if ($lang == LANG_PYTHON) {
            return $source_file;
        }

    }


    function test_solution($user, $password, $id_solution)
    {
        if ($this->authenticate_user($user, $password) != VALID_USER) {
            return array();
        }

        $mysqli = $this->open_db();
        $query  = "SELECT idProblema, codigoFuente, lenguaje FROM Solucion "
                . "WHERE idSolucion = " . $id_solution;
        $result = $mysqli->query($query);
        if ($result->num_rows == 0) {
            $mysqli->close();
            return array();
        }

        $row = $result->fetch_assoc();
        $id_problem = $row['idProblema'];
        $source_code = $row['codigoFuente'];
        $lang = $row['lenguaje'];

        $query  = "SELECT entrada, salida FROM CasoDePrueba "
                . "WHERE idProblema = " . $id_problem;

        $result = $mysqli->query($query);
        if ($result->num_rows == 0) {
            $mysqli->close();
            return array();
        }


        $in_file_name  = tempnam("/tmp", "in_");
        $out_file_name = tempnam("/tmp", "out_");
        $user_output_file = tempnam("/tmp", "user_");
        $veredicts = array();
        $tc = 1;


        while ($row = $result->fetch_assoc()) {
            $input  = $row['entrada'];
            $output = $row['salida'];

            $fin = fopen($in_file_name, "w");
            $fout = fopen($out_file_name, "w");

            fwrite($fin, $input);
            fwrite($fout, $output);

            fclose($fin);
            fclose($fout);

            $executable = $this->compile_program($source_code, $lang);

            $command = "";
            if ($lang == LANG_PYTHON) {
                $command    = "python " . $executable . " < " . $in_file_name
                            . " > " . $user_output_file;

                chmod($in_file_name, 0777);
                chmod($user_output_file, 0777);
            } else {
                $command    = $executable . " < " . $in_file_name
                            . " > " . $user_output_file;
            }

            $exit_code = 0;
            $output = array();

            // The time limit of execution is 1 second.
            $command = "timeout 1s " . $command;
            exec($command, $output, $exit_code);

            // Exit code of timeout command when a program doesn't
            // finish within the specified time.
            if ($exit_code == 124) {
                $veredicts[] = array('id' => $tc, 'error' => TLE); 
            } else {
                $command = "diff --ignore-all-space " . $out_file_name
                    . " " . $user_output_file;
                exec($command, $output, $exit_code);

                if ($exit_code == 0) {
                    $veredicts[] = array('id' => $tc, 'error' => AC); 
                } else {
                    $veredicts[] = array('id' => $tc, 'error' => WA); 
                }
            }

            unlink($executable);
            $tc++;
        }

        unlink($user_output_file);
        unlink($in_file_name);
        unlink($out_file_name);

        $mysqli->close();
        return $veredicts;
    }


    function create_user($user, $password, $remember_token)
    {
        $mysqli = $this->open_db();
        $query = $this->fius($user, $password, $remember_token);
        $mysqli->query($query);
        $id = $mysqli->insert_id;

        if ($mysqli->error == ER_DUP_ENTRY) {
            $mysqli->close();
            return ER_DUP_ENTRY;
        }

        if ($id == 0) {
            return ER_FAILED_TO_INSERT_RECORD;
        }

        return $id;
    }

    function authenticate_user($user, $password)
    {
        $mysqli = $this->open_db();
        $clean_name = utf8_encode(addslashes($user));
        $clean_pass = utf8_encode(addslashes($password));
        $query  = "SELECT nombre, contrasenia FROM Usuario "
            . "WHERE nombre = '" . $clean_name . "' AND "
            . "contrasenia = '" . $clean_pass . "'";

        $result = $mysqli->query($query);

        $mysqli->close();
        if ($result->num_rows == 0) {
            return INVALID_USER;
        }

        return VALID_USER;
    }

    function update_user_remember_token($old, $new)
    {
        $old_rt = utf8_encode(addslashes($old));
        $new_rt = utf8_encode(addslashes($new));
        $query  = "UPDATE Usuario SET remember_token = '" . $new_rt . "' "
                . "WHERE remember_token = '" . $old_rt . "'";
        $mysqli = $this->open_db();
        $mysqli->query($query);
        $mysqli->close();
    }

    // WARNING: If anyone knows the remember_token of the user
    //          then is can get his user and password :(.
    //          I'll improve this later...
    function find_user_by_remember_token($remember_token)
    {
        $rt = utf8_encode(addslashes($remember_token));
        $mysqli = $this->open_db();
        $query  = "SELECT * FROM Usuario WHERE remember_token = '" . $rt . "'";
        $result = $mysqli->query($query);

        $mysqli->close();
        if ($result->num_rows == 0) {
            return array();
        }

        $row = $result->fetch_assoc();
        return array(
            'id' => $row['idUsuario'],
            'name' => $row['nombre'],
            'password' => $row['contrasenia'],
            'remember_token' => $row['remember_token']
        );
    }

    function get_user_data($user, $password)
    {
        if ($this->authenticate_user($user, $password) != VALID_USER) {
            return array();
        }

        $mysqli = $this->open_db();
        $query  = "SELECT idUsuario, remember_token FROM Usuario "
            . "WHERE nombre = '" . $user . "' AND "
            . "contrasenia = '" . $password . "'";
        $result = $mysqli->query($query);

        $mysqli->close();
        if ($result->num_rows == 0) {
            return array();
        }

        $row = $result->fetch_assoc();
        return array(
            'id' => $row['idUsuario'],
            'name' => $user,
            'password' => $password,
            'remember_token' => $row['remember_token']
        );
    }

    function get_user($id_user)
    {
        $mysqli = $this->open_db();
        $query  = "SELECT idUsuario, nombre FROM Usuario "
                . "WHERE idUsuario = " . $id_user;
        $result = $mysqli->query($query);

        if ($result->num_rows == 0) {
            return array();
        }

        $row = $result->fetch_assoc();

        return array(
            'id' => $row['idUsuario'],
            'password' => '',           // NEVER return passwords
            'name' => $row['nombre'],
            'remember_token' => ''      // NOR remember_token
        );
    }

    function get_all_users()
    {
        $mysqli = $this->open_db();
        $query  = "SELECT idUsuario, nombre FROM Usuario "
                . "WHERE nombre != '" . ADMIN_USER . "'";
        $result = $mysqli->query($query);

        if ($result->num_rows == 0) {
            return array();
        }

        $users = array();
        while ($row = $result->fetch_assoc()) {
            $users[] = array(
                'id' => $row['idUsuario'],
                'password' => '',           // NEVER return passwords
                'name' => $row['nombre'],
                'remember_tokeh' => ''      // NOR remember_token
            );
        }

        return $users;
    }
    
}


$server = new soap_server();
$server->configureWSDL("judge", "urn:judge");

$server->wsdl->addComplexType('Veredict', 'complexType', 'struct', 'all', '',
    array(
        'id' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'error' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );

$server->wsdl->addComplexType( 'User', 'complexType', 'struct', 'all', '',
    array(
        'id' => array(
            'type' => 'xsd:integer',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'name' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'password' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'remember_token' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );


$server->wsdl->addComplexType(
    'ArrayOfUser',
    'complexType',
    'array',
    'sequence',
    '',
    array(
        'user' => array(
            'type'      => 'tns:User',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);


$server->wsdl->addComplexType(
    'ArrayOfVeredict',
    'complexType',
    'array',
    'sequence',
    '',
    array(
        'veredict' => array(
            'type'      => 'tns:Veredict',
            'minOccurs' => '1', 
            'maxOccurs' => '1'
        )
    )
);




$server->wsdl->addComplexType('Problem', 'complexType', 'struct', 'all', '',
    array(
        'id' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'title' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1'),
        'description' => array(
            'type' => 'xsd:string',
            'minOccurs' => '1',
            'maxOccurs' => '1')
        )
    );

$server->wsdl->addComplexType(
    'ArrayOfProblem',
    'complexType',
    'array',
    'sequence',
    '',
    array(
        'problem' => array(
            'type'      => 'tns:Problem',
            'minOccurs' => '0', 
            'maxOccurs' => 'unbounded'
        )
    )
);


$server->register('Judge.create_problem',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'title' => 'xsd:string',
        'description' => 'xsd:string',
        'author' => 'xsd:integer'),
    array('return' => 'xsd:integer'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_all_problems',
    array(),
    array(
        'return' => 'tns:ArrayOfProblem'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.save_solution',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'id_problem' => 'xsd:integer',
        'source_code' => 'xsd:string',
        'language' => 'xsd:integer'),
    array('return' => 'tns:Veredict'),
    'http://localhost/microoj_ws/judge.php'
);


$server->register('Judge.compile_solution',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'id_solution' => 'xsd:integer'),
    array('return' => 'xsd:integer'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_last_error',
    array(),
    array('message' => 'xsd:string'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.save_test_case',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'idProblem' => 'xsd:integer',
        'input'     => 'xsd:string',
        'output'    => 'xsd:string'),
    array('return'  => 'xsd:integer'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.test_solution',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'id_solution' => 'xsd:integer'),
    array('return' => 'tns:ArrayOfVeredict'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.create_user',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string',
        'remember_token' => 'xsd:string'),
    array('return' => 'xsd:integer'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_user',
    array('id_user' => 'xsd:integer'),
    array('user' => 'tns:User'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.find_user_by_remember_token',
    array('remember_token' => 'xsd:string'),
    array('user' => 'tns:User'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.update_user_remember_token',
    array('old' => 'xsd:string',
          'new' => 'xsd:string'),
    array(),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_user_data',
    array('user' => 'xsd:string',
          'password' => 'xsd:string'),
    array('user' => 'tns:User'),
    'http://localhost/microoj_ws/judge.php'
);


$server->register('Judge.authenticate_user',
    array(
        'user' => 'xsd:string',
        'password' => 'xsd:string'),
    array('answer' => 'xsd:integer'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_problem',
    array('id_problem' => 'xsd:integer'),
    array(
        'return' => 'tns:Problem'),
    'http://localhost/microoj_ws/judge.php'
);

$server->register('Judge.get_all_users',
    array(),
    array('users' => 'tns:ArrayOfUser'),
    'http://localhost/microoj_ws/judge.php'
);


$server->service($HTTP_RAW_POST_DATA);

?>

